﻿using RemoteServices;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Threading;

namespace PuppetMaster
{
    class PuppetMaster
    {
        static IDictionary pcsServices = new Hashtable();
        public static string ServerUrl { get; set; }
        static Dictionary<string, string> processes = new Dictionary<string, string>();

        static void Main(string[] args)
        {
            ServerUrl = null;
            string[] lines = { };
            if (args.Length > 0)
            {
                try
                {
                    lines = File.ReadAllLines(args[0]);
                }
                catch (IOException ioe)
                {
                    Console.Error.WriteLine(ioe.Message);
                }
            }
            else
            {
                lines = Properties.Resources.StartupScript
                    .Split(new[] { Environment.NewLine }, StringSplitOptions.None);
            }

            foreach (string line in lines)
            {
                string command = line.Trim();
                if (!(command.Length == 0 || command.StartsWith("#")))
                {
                    Console.WriteLine(command);
                    RunCommand(command);
                }
            }

            while (true)
            {
                RunCommand(Console.ReadLine());
            }
        }

        private static void RunCommand(string line)
        {
            string[] command = line.Split(' ');
            switch (command[0])
            {
                case "AddPCS":
                    AddPCS(command[1]);
                    Console.WriteLine("Added PCS at {0}", command[1]);
                    break;
                case "StartClient":
                    Console.WriteLine("Starting Client...");
                    StartClient(command);
                    break;
                case "StartServer":
                    Console.WriteLine("Starting server...");
                    StartServer(command);
                    break;
                case "Freeze":
                    Console.WriteLine("Freezing process...");
                    FreezeProcess(command);
                    break;
                case "UnFreeze":
                    Console.WriteLine("Unfreezing process...");
                    UnFreezeProcess(command);
                    break;
                case "InjectDelay":
                    Console.WriteLine("Injecting el Delay...");
                    InjectDelay(command);
                    break;
                case "Crash":
                    Console.WriteLine("Crashing process...");
                    CrashProcess(command);
                    break;
                case "LocalState":
                    Console.WriteLine("Getting local state...");
                    LocalState(command);
                    break;
                case "Wait":
                    Console.WriteLine("Waiting...");
                    Wait(command);
                    break;
                case "Exit":
                    Console.WriteLine("Exiting gracefully...");
                    Exit();
                    break;
            }
        }

        private static void LocalState(string[] arguments)
        {
            try
            {
                if (((IPCSServices)pcsServices[processes[arguments[1]]]).LocalState(arguments[1], Int32.Parse(arguments[2])))
                {
                    Console.WriteLine("Local state saved successfully");
                }
                else
                    Console.WriteLine("Error getting local state");
            }
            catch (KeyNotFoundException)
            {
                Console.WriteLine("Source Process doesn't exist");
            }
        }

        private static void InjectDelay(string[] arguments)
        {
            try
            {
                if (((IPCSServices)pcsServices[processes[arguments[1]]]).InjectDelay(arguments[1], arguments[2]))
                {
                    Console.WriteLine("Delayed comunication between processes successefully");
                }
                else
                    Console.WriteLine("Error delaying comunications");
            }
            catch (KeyNotFoundException)
            {
                Console.WriteLine("Source Process doesn't exist");
            }
        }

        private static void CrashProcess(string[] arguments)
        {
            try
            {
                if (((IPCSServices)pcsServices[processes[arguments[1]]]).CrashProcess(arguments[1]))
                {
                    Console.WriteLine("Crashed process successefully");
                }
                else
                    Console.WriteLine("Error crashing process");
            }
            catch (KeyNotFoundException)
            {
                Console.WriteLine("Process doesn't exist");
            }

        }

        private static void Exit()
        {
            foreach (IPCSServices pcsService in pcsServices.Values)
            {
                try
                {
                    pcsService.Exit();
                }
                catch (SocketException)
                {
                    //It's fine...
                }
            }
            Environment.Exit(0);
        }

        private static void UnFreezeProcess(string[] arguments)
        {
            if (((IPCSServices)pcsServices[processes[arguments[1]]]).UnFreezeProcess(arguments[1]))
            {
                Console.WriteLine("UnFroze process successefully");
            }
            else
                Console.WriteLine("Error unfreezing process");
        }

        private static void Wait(string[] arguments)
        {
            try
            {
                Console.WriteLine("Waiting");
                Thread.Sleep(Int32.Parse(arguments[1]));
                Console.WriteLine("");
            }
            catch (Exception)
            {
                Console.WriteLine("Error waiting");
            }


        }

        private static void FreezeProcess(string[] arguments)
        {
            if (((IPCSServices)pcsServices[processes[arguments[1]]]).FreezeProcess(arguments[1]))
            {
                Console.WriteLine("Froze process successefully");
            }
            else
                Console.WriteLine("Error freezing process");

        }

        private static void StartClient(string[] arguments)
        {
            string filename;
            if (arguments.Length == 7)
                filename = arguments[6];
            else
                filename = "";
            if (((IPCSServices)pcsServices[arguments[2]]).StartClient(arguments[1], arguments[3], ServerUrl, arguments[4], arguments[5], filename))
            {
                Console.WriteLine("Client started successefully.");
                processes.Add(arguments[1], arguments[2]);
            }
            else
                Console.Write("Error starting client.");
        }

        private static void StartServer(string[] arguments)
        {
            if (ServerUrl != null) //initialize server replica
            {
                if (((IPCSServices)pcsServices[arguments[2]]).StartReplica(arguments[1], arguments[3], arguments[4], arguments[5], ServerUrl))
                {
                    Console.WriteLine("Server replica started successefully.");
                    processes.Add(arguments[1], arguments[2]);
                }
                else
                    Console.Write("Error starting server.");
            }
            else //initialize primary server
            {
                if (((IPCSServices)pcsServices[arguments[2]]).StartServer(arguments[1], arguments[3], arguments[4], arguments[5]))
                {
                    ServerUrl = arguments[3];
                    Console.WriteLine("Server started successefully.");
                    processes.Add(arguments[1], arguments[2]);
                }
                else
                    Console.Write("Error starting server.");
            }

        }


        static void AddPCS(string endpoint)
        {
            pcsServices[endpoint] = ((IPCSServices)Activator.GetObject(typeof(IPCSServices), endpoint));
        }
    }
}
