﻿using RemoteServices;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Serialization.Formatters;
using System.Threading;
using System.Windows.Forms;

namespace Server
{
    public partial class ServerForm : Form
    {
        private static string SERVER_NAME = "Server";
        private static int DELAY_MSEC = 5000;
        private static int NUM_TRIES = 3;
        private static int IM_ALIVE_TIME = 3000;
        public string PId { get; set; }
        public int MsecPerRound { get; set; }
        public int NumPlayers { get; set; }
        public string ServerURL { get; set; }
        private bool gameStarted = false;

        private int connectedPlayers = 0;

        public Dictionary<string, ExternalClient> Clients { get; set; }
        public Dictionary<string, ServerReplica> Servers { get; set; }
        private string nextLeader = "";

        /* Current player information */
        private Dictionary<string, Control> pacmen = new Dictionary<string, Control>();
        Dictionary<string, bool> isDead = new Dictionary<string, bool>();
        private Dictionary<string, Direction> moves = new Dictionary<string, Direction>();
        private Dictionary<string, int> score = new Dictionary<string, int>();

        public Dictionary<string, Position> CoinPositions { get; set; }

        public Dictionary<int, LocalGameState> GameStates { get; set; }
        int roundNumber = 0;


        int readyPlayers = 0;


        /* Game properties */

        int boardRight = 320;
        int boardBottom = 320;
        int boardLeft = 0;
        int boardTop = 40;

        int total_coins = 60;
        int collected_coins = 0;

        //player speed
        int speed = 5;

        //ghost speed for the one direction ghosts
        int ghost1 = 5;
        int ghost2 = 5;

        //x and y directions for the bi-direccional pink ghost
        int ghost3x = 5;
        int ghost3y = 5;




        public ServerForm(string pid, string url, int roundTime, int players, string primaryServerURL)
        {
            InitializeComponent();

            this.PId = pid;
            this.MsecPerRound = roundTime;
            this.NumPlayers = players;
            this.Servers = new Dictionary<string, ServerReplica>();
            this.Clients = new Dictionary<string, ExternalClient>();
            this.GameStates = new Dictionary<int, LocalGameState>();
            this.CoinPositions = new Dictionary<string, Position>();
            this.ServerURL = url;
            PublishServer(url, primaryServerURL);
            timer2.Interval = IM_ALIVE_TIME;
            timer3.Interval = IM_ALIVE_TIME + 400; //TODO: change this, assuming 400ms max network travel time, which is alot
        }

        internal void LocalState(int roundNumber)
        {
            Thread thread = new Thread(() =>
            {

                MessageBox.Show(GameStates[roundNumber].ToString());
                StreamWriter file = File.CreateText(Environment.CurrentDirectory
                    + String.Format("/../../../Outputs/LocalState-{0}-{1}.txt", PId, roundNumber));
                file.Write(GameStates[roundNumber]);
                file.Close();

            });
            thread.Start();
        }

        internal void DelayMessage(string destPID)
        {
            try
            {
                this.Clients[destPID].Delay = true;
            }
            catch (KeyNotFoundException ke)
            {
                //TODO: try delay server i guess
                //Client does not exist

            }
        }

        private void PublishServer(string url, string primaryURL)
        {

            BinaryServerFormatterSinkProvider provider = new BinaryServerFormatterSinkProvider();
            provider.TypeFilterLevel = TypeFilterLevel.Full;

            IDictionary properties = new Hashtable();
            properties["name"] = "tcp:server" + PId;
            properties["port"] = url.Split(':')[2].Split('/')[0];

            TcpChannel channel = new TcpChannel(properties, null, provider);
            ChannelServices.RegisterChannel(channel, false);
            RemotingConfiguration.RegisterWellKnownServiceType(
                typeof(ServerServices),
                SERVER_NAME,
                WellKnownObjectMode.Singleton);

            IServerServices self = (IServerServices)Activator.GetObject(typeof(IServerServices), url);

            if (url == primaryURL)
            {
                this.Text = "Primary DADman Server - " + PId;
                Servers.Add(this.PId, new ServerReplica(self, true));
                tbChat.AppendText("Registered as primary server." + Environment.NewLine);
            }
            else
            {
                this.Text = "Secondary DADman Server - " + PId;
                try
                {
                    RegisterToMainServer(primaryURL, self);
                }
                catch (SocketException)
                {
                    //TODO: handle this
                }
            }

            timer2.Start();
            timer3.Start();

        }



        public void RegisterClient(string endpoint, string name)
        {

            //TODO verify number of players

            IClientServices newClient = (IClientServices)Activator.GetObject(typeof(IClientServices), endpoint);

            ExternalClient externalClient = new ExternalClient(newClient);
            Clients.Add(name, externalClient);
            moves.Add(name, Direction.NONE);
            AddPacman(name, ++connectedPlayers);
            score.Add(name, 0);
            isDead.Add(name, false);

            tbChat.AppendText(name + " has connected" + Environment.NewLine);

            foreach (KeyValuePair<string, ServerReplica> server in Servers)
            {
                if (server.Value.Disconnected || server.Key == this.PId) continue;
                Thread thread = new Thread(() => UpdateClientInfoHandler(server.Key, new KeyValuePair<string, ExternalClient>(name, externalClient)));
                thread.Start();
            }

            if (connectedPlayers == NumPlayers)
            {
                foreach (Control x in this.Controls)
                {
                    if (x is PictureBox && x.Tag == "coin")
                    {
                        this.CoinPositions.Add(x.Name, new Position(((PictureBox)x).Location.X, ((PictureBox)x).Location.Y));
                    }
                }

                Dictionary<string, Position> positions = new Dictionary<string, Position>();
                foreach (KeyValuePair<string, Control> item in pacmen)
                {
                    positions.Add(item.Key, new Position(item.Value.Location.X, item.Value.Location.Y));
                }

                foreach (KeyValuePair<string, ExternalClient> client in Clients)
                {
                    if (client.Value.Disconnected) continue;

                    Thread thread = new Thread(() => SendGameInfoHandler(client.Key, positions));
                    thread.Start();
                }

                //wait for all players to be ready
                while (readyPlayers != connectedPlayers) ;

                foreach (KeyValuePair<string, ExternalClient> item in Clients)
                {
                    if (item.Value.Disconnected) continue;
                    Thread thread = new Thread(() => DelayHandler(delegate () { SignalGameStartHandler(item.Key); }, item.Key));
                    thread.Start();
                }

                gameStarted = true;
                timer1.Interval = this.MsecPerRound;
                timer1.Start();
            }

        }

        /* Handle delay */
        private void DelayHandler(MethodInvoker d, string pId)
        {
            if (Clients[pId].Delay)
            {
                Thread.Sleep(DELAY_MSEC);
                //d.Method.Invoke(d.Target,)

            }
            d.Method.Invoke(d.Target, d.Method.GetParameters());
        }

        private void SendGameInfoHandler(string pId, Dictionary<string, Position> positions)
        {
            try
            {
                Clients[pId].ClientServices.SendGameInfo(positions);
                readyPlayers++;
            }
            catch (SocketException)
            {
                tbChat.AppendText("Client " + pId + " has disconnected unexpectedly." + Environment.NewLine);
                Clients[pId].Disconnected = true;
                this.Controls.Remove(pacmen[pId]);
                this.isDead[pId] = true;
                connectedPlayers--;
                PropagateClientInfo(pId);
            }
        }

        private void SignalGameStartHandler(string pId)
        {
            try
            {
                Clients[pId].ClientServices.SignalGameStart();
            }
            catch (SocketException)
            {
                tbChat.AppendText("Client " + pId + " has disconnected unexpectedly." + Environment.NewLine);
                Clients[pId].Disconnected = true;
                this.Controls.Remove(pacmen[pId]);
                this.isDead[pId] = true;
                connectedPlayers--;
                PropagateClientInfo(pId);
            }

        }


        public void AddMove(string client, Direction direction)
        {
            moves[client] = direction;
        }

        private void AddPacman(string pId, int i)
        {
            PictureBox pacman = new PictureBox
            {
                BackColor = Color.Transparent,
                Image = Properties.Resources.Left,
                Location = new Point(8, (i * 40)),
                Margin = new Padding(0),
                Name = pId,
                Size = new Size(25, 25),
                SizeMode = PictureBoxSizeMode.StretchImage,
                TabIndex = 4,
                TabStop = false
            };
            this.Controls.Add(pacman);
            this.pacmen.Add(pId, pacman);

        }

        private void EndTheGame()
        {
            var toList = score.ToList();
            toList.Sort((pair1, pair2) => pair2.Value.CompareTo(pair1.Value));
            KeyValuePair<string, int> winner = toList[0];
            tbChat.AppendText(String.Format("{0} has won the game with {1} points!{2}",
                winner.Key, winner.Value, Environment.NewLine));

            foreach (KeyValuePair<string, ExternalClient> client in Clients)
            {
                if (client.Value.Disconnected) continue;
                Thread thread = new Thread(() => SignalGameEndHandler(client.Key, winner));
                thread.Start();
            }
        }

        private void SignalGameEndHandler(string pId, KeyValuePair<string, int> winner)
        {
            try
            {
                Clients[pId].ClientServices.SignalGameEnd(winner.Key, winner.Value);
            }
            catch (SocketException)
            {
                tbChat.AppendText("Client " + pId + " has disconnected unexpectedly." + Environment.NewLine);
                Clients[pId].Disconnected = true;
                this.Controls.Remove(pacmen[pId]);
                this.isDead[pId] = true;
                connectedPlayers--;
                PropagateClientInfo(pId);
            }
        }



        private void timer1_Tick(object sender, EventArgs e)
        {
            if (connectedPlayers == 0)
            {
                tbChat.AppendText("All players have disconnected, the game will now stop." + Environment.NewLine);
            }
            int dead = 0;
            List<string> coinsToRemove = new List<string>();

            foreach (KeyValuePair<string, Control> pacman in pacmen)
            {
                if (isDead[pacman.Key])
                {
                    dead++;
                    continue;
                }

                foreach (Control x in this.Controls)
                {
                    // checking if the player hits the wall or the ghosts
                    if (x is PictureBox && x.Tag == "wall" || x.Tag == "ghost")
                    {
                        if (((PictureBox)x).Bounds.IntersectsWith(pacman.Value.Bounds))
                        {
                            isDead[pacman.Key] = true;
                            this.Controls.Remove(pacman.Value);
                            tbChat.AppendText(pacman.Key + " has died" + Environment.NewLine);
                            break;
                        }
                        else
                        {
                            isDead[pacman.Key] = false;
                        }
                    }
                    if (x is PictureBox && x.Tag == "coin")
                    {
                        if (((PictureBox)x).Bounds.IntersectsWith(pacman.Value.Bounds))
                        {
                            coinsToRemove.Add(x.Name);
                            this.Controls.Remove(x);
                            this.CoinPositions.Remove(x.Name);
                            score[pacman.Key]++;
                            collected_coins++;
                        }
                    }
                }
            }
            if (dead >= NumPlayers)
            {
                this.EndTheGame();
                timer1.Stop();
            }


            //move players
            foreach (KeyValuePair<string, Control> pacman in pacmen)
            {
                if (isDead[pacman.Key]) continue;
                switch (moves[pacman.Key])
                {
                    case Direction.LEFT:
                        if (pacman.Value.Left > (boardLeft))
                        {
                            ((PictureBox)pacman.Value).Image = Properties.Resources.Left;
                            pacman.Value.Left -= speed;
                        }
                        break;
                    case Direction.RIGHT:
                        if (pacman.Value.Left < (boardRight))
                        {
                            ((PictureBox)pacman.Value).Image = Properties.Resources.Right;
                            pacman.Value.Left += speed;
                        }
                        break;
                    case Direction.UP:
                        if (pacman.Value.Top > (boardTop))
                        {
                            ((PictureBox)pacman.Value).Image = Properties.Resources.Up;
                            pacman.Value.Top -= speed;
                        }
                        break;
                    case Direction.DOWN:
                        if (pacman.Value.Top < (boardBottom))
                        {
                            ((PictureBox)pacman.Value).Image = Properties.Resources.down;
                            pacman.Value.Top += speed;
                        }
                        break;
                    case Direction.NONE:
                        break;
                    default:
                        break;
                }
            }

            //move ghosts
            redGhost.Left += ghost1;
            yellowGhost.Left += ghost2;

            // if the red ghost hits the picture box 4 then wereverse the speed
            if (redGhost.Bounds.IntersectsWith(pictureBox1.Bounds))
                ghost1 = -ghost1;
            // if the red ghost hits the picture box 3 we reverse the speed
            else if (redGhost.Bounds.IntersectsWith(pictureBox2.Bounds))
                ghost1 = -ghost1;
            // if the yellow ghost hits the picture box 1 then wereverse the speed
            if (yellowGhost.Bounds.IntersectsWith(pictureBox3.Bounds))
                ghost2 = -ghost2;
            // if the yellow chost hits the picture box 2 then wereverse the speed
            else if (yellowGhost.Bounds.IntersectsWith(pictureBox4.Bounds))
                ghost2 = -ghost2;

            pinkGhost.Left += ghost3x;
            pinkGhost.Top += ghost3y;

            if (pinkGhost.Left < boardLeft ||
                pinkGhost.Left > boardRight ||
                (pinkGhost.Bounds.IntersectsWith(pictureBox1.Bounds)) ||
                (pinkGhost.Bounds.IntersectsWith(pictureBox2.Bounds)) ||
                (pinkGhost.Bounds.IntersectsWith(pictureBox3.Bounds)) ||
                (pinkGhost.Bounds.IntersectsWith(pictureBox4.Bounds)))
            {
                ghost3x = -ghost3x;
            }
            if (pinkGhost.Top < boardTop || pinkGhost.Top + pinkGhost.Height > boardBottom - 2)
            {
                ghost3y = -ghost3y;
            }



            this.label1.Text = "Coins remaining: " + (total_coins - collected_coins);
            if (collected_coins == total_coins)
            {
                this.EndTheGame();
                timer1.Stop();
            }


            //Create GameState
            Dictionary<string, Position> positions = new Dictionary<string, Position>();
            foreach (KeyValuePair<string, Control> item in pacmen)
            {
                positions.Add(item.Key, new Position(item.Value.Location.X, item.Value.Location.Y));
            }

            //Ghost positions
            Dictionary<Ghost, Position> ghostPositions = new Dictionary<Ghost, Position>();
            var ghosts = Enum.GetValues(typeof(Ghost)).Cast<Ghost>();
            foreach (Ghost item in ghosts)
            {
                Position position = null;
                switch (item)
                {
                    case Ghost.PINK:
                        position = new Position(pinkGhost.Location.X, pinkGhost.Location.Y);
                        break;
                    case Ghost.RED:
                        position = new Position(redGhost.Location.X, redGhost.Location.Y);
                        break;
                    case Ghost.YELLOW:
                        position = new Position(yellowGhost.Location.X, yellowGhost.Location.Y);
                        break;
                    default:
                        break;
                }
                ghostPositions.Add(item, position);
            }

            Dictionary<string, ClientInfo> clientsInfo = new Dictionary<string, ClientInfo>();
            foreach (KeyValuePair<string, ExternalClient> client in Clients)
            {
                ClientInfo info = new ClientInfo(
                    new Position(pacmen[client.Key].Location.X, pacmen[client.Key].Location.Y),
                    score[client.Key],
                    isDead[client.Key],
                    moves[client.Key]);
                clientsInfo.Add(client.Key, info);
            }

            GameStateChanges newState = new GameStateChanges(++roundNumber, clientsInfo, ghostPositions, coinsToRemove);

            foreach (KeyValuePair<string, ExternalClient> client in Clients)
            {
                if (client.Value.Disconnected) continue;
                Thread thread = new Thread(() => DelayHandler(delegate () { SendStateClientHandler(client.Key, newState); }, client.Key));
                thread.Start();
            }

            foreach (KeyValuePair<string, ServerReplica> server in Servers)
            {
                if (server.Value.Disconnected || server.Key == this.PId) continue;
                Thread thread = new Thread(() => SendStateServerHandler(server.Key, newState));
                thread.Start();

            }

            LocalGameState localState = new LocalGameState(newState, this.CoinPositions.Values.ToList());
            this.GameStates.Add(newState.RoundNumber, localState);


            //reset players' directions
            Dictionary<string, Direction> temp = new Dictionary<string, Direction>();
            foreach (KeyValuePair<string, Direction> pacmanMoves in moves)
            {
                temp.Add(pacmanMoves.Key, Direction.NONE);
            }
            moves = temp;

        }

        private void SendStateClientHandler(string pId, GameStateChanges state)
        {
            try
            {
                Clients[pId].ClientServices.SendState(state);
            }
            catch (SocketException)
            {
                if (Clients[pId].Disconnected) return;
                Clients[pId].Disconnected = true;
                this.Controls.Remove(pacmen[pId]);
                this.isDead[pId] = true;
                connectedPlayers--;
                tbChat.AppendText("Client " + pId + " has disconnected unexpectedly." + Environment.NewLine);
                PropagateClientInfo(pId);
            }
        }

        private void SendStateServerHandler(string pId, GameStateChanges state)
        {
            try
            {
                Servers[pId].ServerServices.updateInternalState(state);
            }
            catch (SocketException)
            {
                //TODO: handle this
            }
        }
















        /* Redundancy */

        public void AddReplica(string pId, IServerServices server)
        {
            this.Servers.Add(pId, new ServerReplica(server, false));
            tbChat.AppendText(pId + " has been registered as a secondary server." + Environment.NewLine);
            foreach (KeyValuePair<string, ServerReplica> replica in Servers)
            {
                if (replica.Key == this.PId) continue;
                Thread thread = new Thread(() => UpdateReplicasHandler(replica.Key, Servers));
                thread.Start();
            }
        }

        public void UpdateReplicasHandler(string pId, Dictionary<string, ServerReplica> servers)
        {
            try
            {
                Servers[pId].ServerServices.UpdateReplicas(servers);
            }
            catch (SocketException)
            {
                if (++Servers[pId].Tries == NUM_TRIES)
                {
                    Servers[pId].Disconnected = true;
                    tbChat.AppendText(pId + " is presumed dead." + Environment.NewLine);
                }
                else
                {
                    UpdateReplicasHandler(pId, servers);
                }
            }
        }


        public void RegisterToMainServer(string serverEndpoint, IServerServices newReplica)
        {
            IServerServices server = (IServerServices)Activator.GetObject(typeof(IServerServices), serverEndpoint);

            try
            {
                server.RegisterReplica(this.PId, newReplica);
                tbChat.AppendText("Registered as secondary server." + Environment.NewLine);
            }
            catch (Exception e)
            {
                tbChat.AppendText("Failure contacting primary server." + Environment.NewLine);
                MessageBox.Show(e.Message);
            }
        }


        public void UpdateReplicas(Dictionary<string, ServerReplica> replicas)
        {
            this.Servers = replicas;
        }

        //send imalive to the servers
        private void timer2_Tick(object sender, EventArgs e)
        {
            foreach (KeyValuePair<string, ServerReplica> server in Servers)
            {
                if (server.Value.Disconnected || server.Key == this.PId || server.Value.IsPrimary) continue;
                Thread thread = new Thread(() => ImAliveHandler(server.Key));
                thread.Start();
            }
        }

        private void ImAliveHandler(string pId)
        {
            try
            {
                Servers[pId].ServerServices.ImAlive(this.PId);
                Servers[pId].Tries = 0;
            }
            catch (SocketException)
            {
                if (++Servers[pId].Tries == NUM_TRIES)
                {
                    Servers[pId].Disconnected = true;
                    tbChat.AppendText(pId + " is presumed dead." + Environment.NewLine);
                }
                else
                {
                    ImAliveHandler(pId);
                }
            }
        }

        //verify if all servers are alive
        private void timer3_Tick(object sender, EventArgs e)
        {
            if (Servers[this.PId].IsPrimary) return;
            foreach (KeyValuePair<string, ServerReplica> server in Servers)
            {
                if (server.Value.Disconnected || server.Key == this.PId) continue;
                if (!server.Value.IsAlive)
                {
                    server.Value.Disconnected = true;
                    tbChat.AppendText(server.Key + " is presumed dead." + Environment.NewLine);
                    if (server.Value.IsPrimary)
                    {
                        server.Value.IsPrimary = false;
                        Consensus();
                        break;
                    }
                }
                else
                {
                    server.Value.IsAlive = false;
                }

            }
        }

        public void UpdateClient(KeyValuePair<string, ExternalClient> client)
        {
            if (Clients.ContainsKey(client.Key))
            {
                Clients[client.Key] = client.Value;
                if (Clients[client.Key].Disconnected)
                {
                    connectedPlayers--;
                    tbChat.AppendText("Client " + client.Key + " has disconnected unexpectedly."
                        + Environment.NewLine);
                }
            }
            else
            {
                Clients.Add(client.Key, client.Value);
                moves.Add(client.Key, Direction.NONE);
                AddPacman(client.Key, ++connectedPlayers);
                score.Add(client.Key, 0);
                isDead.Add(client.Key, false);
                tbChat.AppendText(client.Key + " has connected" + Environment.NewLine);
            }
        }




        public void UpdateState(GameStateChanges state)
        {
            if (!gameStarted) gameStarted = true;

            this.roundNumber = state.RoundNumber;
            foreach (KeyValuePair<string, ExternalClient> item in Clients)
            {
                isDead[item.Key] = state.Clients[item.Key].IsDead;
            }

            foreach (KeyValuePair<string, ClientInfo> info in state.Clients)
            {
                pacmen[info.Key].Location = new Point(info.Value.Position.X, info.Value.Position.Y);
                if (info.Value.IsDead)
                {
                    this.Controls.Remove(pacmen[info.Key]);
                }
                switch (info.Value.Direction)
                {
                    case Direction.LEFT:
                        ((PictureBox)pacmen[info.Key]).Image = Properties.Resources.Left;
                        break;
                    case Direction.RIGHT:
                        ((PictureBox)pacmen[info.Key]).Image = Properties.Resources.Right;
                        break;
                    case Direction.UP:
                        ((PictureBox)pacmen[info.Key]).Image = Properties.Resources.Up;
                        break;
                    case Direction.DOWN:
                        ((PictureBox)pacmen[info.Key]).Image = Properties.Resources.down;
                        break;
                    case Direction.NONE:
                        break;
                    default:
                        break;
                }

                score[info.Key] = info.Value.Score;
            }

            //Update ghosts' positions
            foreach (KeyValuePair<Ghost, Position> ghost in state.GhostsPositions)
            {
                switch (ghost.Key)
                {
                    case Ghost.PINK:
                        pinkGhost.Location = new Point(ghost.Value.X, ghost.Value.Y);
                        break;
                    case Ghost.RED:
                        redGhost.Location = new Point(ghost.Value.X, ghost.Value.Y);
                        break;
                    case Ghost.YELLOW:
                        yellowGhost.Location = new Point(ghost.Value.X, ghost.Value.Y);
                        break;
                    default:
                        break;
                }
            }

            //Remove "eaten" coins
            foreach (string coinName in state.CoinsToRemove)
            {
                this.Controls.Remove(this.Controls.Find(coinName, false)[0]);
                this.CoinPositions.Remove(coinName);
                collected_coins++;
            }
        }




        public void UpdateClientInfoHandler(string pId, KeyValuePair<string, ExternalClient> client)
        {
            try
            {
                Servers[pId].ServerServices.UpdateClientInfo(client);
            }
            catch (SocketException)
            {
                //TODO: handle this
            }
        }


        public void Consensus()
        {
            timer2.Stop();
            timer3.Stop();
            bool leaderFound = false;
            List<string> list = new List<string>();
            string next = "";

            foreach (KeyValuePair<string, ServerReplica> server in Servers)
            {
                if (server.Value.IsAlive) list.Add(server.Key);
            }
            list.Sort();
            for (int i = 0; i < list.Count; i++)
            {
                if (i == list.Count - 1)
                {
                    next = list[0];
                    break;
                }
                else if (list[i] == this.PId)
                {
                    next = list[i + 1];
                    break;
                }
            }
            string leader = list[0];

            while (!leaderFound)
            {
                try
                {
                    Servers[next].ServerServices.ChooseLeader(leader);
                }
                catch (SocketException)
                {
                    //TODO: handle this
                }

                if (leader != nextLeader && nextLeader != "")
                {
                    leader = nextLeader;
                }
                else
                {
                    leaderFound = true;
                    Servers[leader].IsPrimary = true;
                    if (this.PId == leader)
                    {
                        this.Text = "Primary DADman Server - " + PId;
                        RestartGame();
                    }
                    tbChat.AppendText("The next primary server is " + leader
                        + "." + Environment.NewLine);
                }
            }

            nextLeader = "";
            timer2.Start();
            timer3.Start();

        }

        public void RestartGame()
        {
            if (!gameStarted) return;
            readyPlayers = 0;
            foreach (KeyValuePair<string, ExternalClient> client in Clients)
            {
                Thread thread = new Thread(() => NotifyClientHandler(client.Key));
                thread.Start();
            }

            //wait for all players to be ready
            while (readyPlayers != connectedPlayers) ;

            foreach (KeyValuePair<string, ExternalClient> item in Clients)
            {
                if (item.Value.Disconnected) continue;
                Thread thread = new Thread(() => DelayHandler(delegate () { SignalGameStartHandler(item.Key); }, item.Key));
                thread.Start();
            }

            timer1.Interval = this.MsecPerRound;
            timer1.Start();
        }

        public void NotifyClientHandler(string pId)
        {
            try
            {
                Clients[pId].ClientServices.NotifyNewServer(ServerURL);
                readyPlayers++;
            }
            catch (SocketException)
            {
                //TODO
            }
        }

        public void ChangeLeader(string PId)
        {
            this.nextLeader = PId;
        }

        public void SignalRingStartHandler(string pId)
        {
            try
            {
                Servers[pId].ServerServices.SignalRingStart();
            }
            catch (SocketException)
            {
                if (++Servers[pId].Tries == NUM_TRIES)
                {
                    Servers[pId].Disconnected = true;
                    tbChat.AppendText(pId + " is presumed dead." + Environment.NewLine);
                }
                else
                {
                    SignalRingStartHandler(pId);
                }
            }
        }

        public void PropagateClientInfo(string clientPId)
        {
            foreach (KeyValuePair<string, ServerReplica> server in Servers)
            {
                if (server.Value.Disconnected || server.Key == this.PId) continue;
                Thread thread = new Thread(() => UpdateClientInfoHandler(server.Key, new KeyValuePair<string, ExternalClient>(clientPId, Clients[clientPId])));
                thread.Start();
            }
        }


    }

}
