﻿using RemoteServices;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Server
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new ServerForm(args[0], args[1], Int32.Parse(args[2]), Int32.Parse(args[3]), args[4]));

        }
    }


    class ServerServices : MarshalByRefObject, IServerServices
    {

        bool freeze = false;
        private object unfreezing = new object();

        private Queue<MethodInvoker> toProcess = new Queue<MethodInvoker>();

        public void AddClient(string endpoint, string name)
        {
            ServerForm form = (ServerForm)Application.OpenForms[0];
            MethodInvoker mI = delegate () { form.RegisterClient(endpoint, name); };
            if (freeze) toProcess.Enqueue(mI);
            else form.BeginInvoke(mI);
        }

        public Dictionary<string, ExternalClient> getConnectedClients()
        {
            ServerForm form = (ServerForm)Application.OpenForms[0];
            while (freeze) ;
            return form.Clients;
        }

        public string getPID()
        {
            ServerForm form = (ServerForm)Application.OpenForms[0];
            while (freeze) ;
            return form.PId;
        }

        public void ImAlive(string pId)
        {
            ServerForm form = (ServerForm)Application.OpenForms[0];
            MethodInvoker mI = delegate () { form.Servers[pId].IsAlive = true; form.Servers[pId].Disconnected = false; };
            form.BeginInvoke(mI);
            //TODO: freeze
        }

        public void LocalState(int roundNumber)
        {
            ServerForm form = (ServerForm)Application.OpenForms[0];
            MethodInvoker mI = delegate () { form.LocalState(roundNumber); };
            form.Invoke(mI);
        }

        public void Move(string client, Direction direction)
        {
            ServerForm form = (ServerForm)Application.OpenForms[0];
            MethodInvoker mI = delegate () { form.AddMove(client, direction); };
            if (freeze) toProcess.Enqueue(mI);
            else form.BeginInvoke(mI);
        }

        public void RegisterReplica(string pId, IServerServices newReplica)
        {
            ServerForm form = (ServerForm)Application.OpenForms[0];
            MethodInvoker mI = delegate () { form.AddReplica(pId, newReplica); };
            form.BeginInvoke(mI);
            //TODO: freeze
        }

        public void SignalDelay(string destPID)
        {
            ServerForm form = (ServerForm)Application.OpenForms[0];
            MethodInvoker mI = delegate () { form.DelayMessage(destPID); };
            form.BeginInvoke(mI);
        }

        public void SignalFreeze()
        {
            freeze = true;
        }

        public void SignalRingStart()
        {
            ServerForm form = (ServerForm)Application.OpenForms[0];
            MethodInvoker mI = delegate () { form.Consensus(); };
            form.BeginInvoke(mI);
            //TODO: freeze
        }

        public void SignalUnFreeze()
        {
            lock (unfreezing)
            {
                ServerForm form = (ServerForm)Application.OpenForms[0];
                for (int i = 0; i < toProcess.Count; i++)
                {
                    form.Invoke(toProcess.Dequeue());
                }
                freeze = false;
            }
        }

        public void UpdateClientInfo(KeyValuePair<string, ExternalClient> client)
        {
            ServerForm form = (ServerForm)Application.OpenForms[0];
            MethodInvoker mI = delegate () { form.UpdateClient(client); };
            form.BeginInvoke(mI);
            //TODO: freeze
        }

        public void updateInternalState(GameStateChanges newState)
        {
            ServerForm form = (ServerForm)Application.OpenForms[0];
            MethodInvoker mI = delegate () { form.UpdateState(newState); };
            form.BeginInvoke(mI);
            //TODO: freeze
        }

        public void UpdateReplicas(Dictionary<string, ServerReplica> servers)
        {
            ServerForm form = (ServerForm)Application.OpenForms[0];
            MethodInvoker mI = delegate () { form.UpdateReplicas(servers); };
            form.BeginInvoke(mI);
            //TODO: freeze
        }

        public void ChooseLeader(string leaderPId)
        {
            ServerForm form = (ServerForm)Application.OpenForms[0];
            MethodInvoker mI = delegate () { form.ChangeLeader(leaderPId); };
            form.BeginInvoke(mI);
            //TODO: freeze
        }
    }

}
