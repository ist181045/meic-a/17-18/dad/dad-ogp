﻿using RemoteServices;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Threading;
using System.Windows.Forms;

namespace Client
{
    public partial class ClientForm : Form
    {

        private static string CLIENT_NAME = "Client";
        private static int NUM_TRIES = 3;

        private string serverPID;
        private IServerServices server;
        private Dictionary<string, ExternalClient> clients = new Dictionary<string, ExternalClient>();
        public int MsecPerRound { get; set; }
        public int NumPlayers { get; set; }
        public string PId { get; set; }
        public string ServerEndpoint { get; set; }
        private int roundNumber = 0; //TODO: (maybe?) send it to the server as verification on which packages to discard


        /* PuppetMaster */
        private bool serverDelay = false;
        private static int DELAY_TIME = 2000;
        private bool freeze = false;
        public Dictionary<int, LocalGameState> GameStates { get; set; }
        public Dictionary<string, Position> CoinPositions { get; set; }
        private bool fileInput = false;
        private SortedDictionary<int, Direction> inputMoves = new SortedDictionary<int, Direction>();



        /* Messages */
        private Dictionary<string, int> timestamps = new Dictionary<string, int>();
        private List<ChatMessage> messages = new List<ChatMessage>();


        /* Game */
        private Dictionary<string, Control> pacmen = new Dictionary<string, Control>();
        private Dictionary<string, int> score = new Dictionary<string, int>();

        private bool isDead = false;

        bool goup;
        bool godown;
        bool goleft;
        bool goright;





        public ClientForm(string[] args)
        {

            InitializeComponent();

            //in case it has to get his actions from specefied trace file
            if (args.Length == 6)
            {
                string[] lines = { };
                if (args.Length == 6)
                {
                    try
                    {
                        lines = File.ReadAllLines(args[5]);
                    }
                    catch (IOException ioe)
                    {
                        Console.Error.WriteLine(ioe.Message);
                    }
                }

                foreach (string str in lines)
                {
                    string[] cmd = str.Split(',');
                    switch (cmd[1])
                    {
                        case "LEFT":
                            inputMoves.Add(Int32.Parse(cmd[0]), Direction.LEFT);
                            break;
                        case "RIGHT":
                            inputMoves.Add(Int32.Parse(cmd[0]), Direction.RIGHT);
                            break;
                        case "UP":
                            inputMoves.Add(Int32.Parse(cmd[0]), Direction.UP);
                            break;
                        case "DOWN":
                            inputMoves.Add(Int32.Parse(cmd[0]), Direction.DOWN);
                            break;
                        default:
                            break;
                    }
                }
                fileInput = true;
            }

            this.GameStates = new Dictionary<int, LocalGameState>();
            this.CoinPositions = new Dictionary<string, Position>();

            ConnectToServer(args);

        }

        internal void LocalState(int roundNumber)
        {
            Thread thread = new Thread(() =>
            {

                MessageBox.Show(GameStates[roundNumber].ToString());
                StreamWriter file = File.CreateText(
                    String.Format("{0}/../../../Outputs/LocalState-{1}-{2}.txt",
                        Environment.NewLine, PId, roundNumber));
                file.Write(GameStates[roundNumber]);
                file.Close();

            });
            thread.Start();

        }


        public void GameStartInfo(Dictionary<string, Position> positions)
        {
            this.Text += " - " + PId;


            clients = server.getConnectedClients();

            foreach (KeyValuePair<string, ExternalClient> client in clients)
            {
                this.timestamps.Add(client.Key, 0);
                this.score.Add(client.Key, 0);
                AddPacman(client.Key, positions[client.Key]);
                if (client.Value.Disconnected || client.Key == this.PId) continue;
                Thread thread = new Thread(() => PingClient(client.Key));
                thread.Start();
            }
            clients.Remove(this.PId);

            foreach (Control x in this.Controls)
            {
                if (x is PictureBox && x.Tag == "coin")
                {
                    this.CoinPositions.Add(x.Name, new Position(((PictureBox)x).Location.X, ((PictureBox)x).Location.Y));
                }
            }

        }

        public void PingClient(string clientPId)
        {
            try
            {
                clients[clientPId].ClientServices.Ping();
                clients[clientPId].Tries = 0;
            }
            catch (SocketException se)
            {

                if (++clients[clientPId].Tries == NUM_TRIES)
                {
                    tbChat.AppendText(String.Format("Client {0} has disconnected unexpectedly.{1}",
                        clientPId, Environment.NewLine));
                    clients[clientPId].Disconnected = true;
                    this.Controls.Remove(pacmen[clientPId]);
                }
                else
                {
                    //TODO: add delay, maybe?
                    PingClient(clientPId);
                }
            }
        }

        public void StartGame()
        {
            timer1.Interval = this.MsecPerRound;
            timer1.Start();
        }

        public void GameEnd(string winner, int points)
        {
            tbChat.AppendText(String.Format("The winner is {0} with {1} points!{2}",
                winner, points, Environment.NewLine));
        }

        internal void BlockChat()
        {
            freeze = true;
        }

        internal void UnBlockChat()
        {
            freeze = false;
        }

        public void FreezeGame()
        {

        }

        public void AddMessage(string origin, string message, Dictionary<string, int> vector)
        {
            messages.Add(new ChatMessage(origin, message, vector));
            PrintMessages();
        }

        internal void DelayMessage(string destPID)
        {
            if (clients.ContainsKey(destPID))
            {
                //clientDelay 
                clients[destPID].Delay = true;

            }
            else if (serverPID.Equals(destPID))
            {
                serverDelay = true;
            }
        }

        private void PrintMessages()
        {
            bool print = true;

            foreach (ChatMessage m in messages)
            {
                foreach (KeyValuePair<string, int> ts in m.Vector)
                {
                    if (ts.Key == this.PId)
                        continue;
                    if (ts.Key == m.Origin && ts.Value != timestamps[ts.Key] + 1)
                    {
                        print = false;
                        break;
                    }
                    else if (ts.Value < timestamps[ts.Key])
                    {
                        print = false;
                        break;
                    }
                }

                if (print)
                {
                    if (tbChat.Text != "") tbChat.AppendText(Environment.NewLine);
                    tbChat.AppendText(String.Format("[{0}] {1}", m.Origin, m.Content));
                    timestamps[m.Origin] = m.Vector[m.Origin];
                    messages.Remove(m);
                    PrintMessages();
                    return;
                }
                print = true;
            }
        }

        public void ConnectToServer(string[] args)
        {
            PId = args[0];
            string clientUrl = args[1];
            ServerEndpoint = args[2];
            MsecPerRound = Int32.Parse(args[3]);
            NumPlayers = Int32.Parse(args[4]);

            server = (IServerServices)Activator.GetObject(typeof(IServerServices), ServerEndpoint);
            try
            {
                serverPID = server.getPID();
            }
            catch (Exception e)
            {
                //TODO
            }



            IDictionary properties = new Hashtable();
            properties["name"] = "tcp:client" + PId;
            properties["port"] = clientUrl.Split(':')[2].Split('/')[0];

            TcpChannel channel = new TcpChannel(properties, null, null);

            ChannelServices.RegisterChannel(channel, false);
            RemotingConfiguration.RegisterWellKnownServiceType(
                typeof(ClientServices),
                CLIENT_NAME,
                WellKnownObjectMode.Singleton);

            server.AddClient(clientUrl, PId);
        }

        public void ReconnectToServer(string endpoint)
        {
            ServerEndpoint = endpoint;
            server = (IServerServices)Activator.GetObject(typeof(IServerServices), ServerEndpoint);
            try
            {
                serverPID = server.getPID();
            }
            catch (Exception)
            {
                //TODO
            }
        }



        private void ChatSendMessage(string clientPId, string message, Dictionary<string, int> timestamps)
        {
            try
            {
                if (clients[clientPId].Delay) Thread.Sleep(DELAY_TIME);
                clients[clientPId].ClientServices.SendMsg(this.PId, message, timestamps);
                clients[clientPId].Tries = 0;
            }
            catch (SocketException se)
            {
                if (++clients[clientPId].Tries == NUM_TRIES)
                {
                    tbChat.AppendText(String.Format("Client {0} has disconnected unexpectedly.{1}",
                        clientPId, Environment.NewLine));
                    clients[clientPId].Disconnected = true;
                    this.Controls.Remove(pacmen[clientPId]);
                }
                else
                {
                    //TODO: add delay, maybe?
                    ChatSendMessage(clientPId, message, timestamps);
                }
            }
        }

        public void UpdateGameState(GameStateChanges state)
        {

            isDead = state.Clients[this.PId].IsDead;
            if (isDead) this.label2.Text = "Game Over";

            foreach (KeyValuePair<string, ClientInfo> info in state.Clients)
            {
                pacmen[info.Key].Location = new Point(info.Value.Position.X, info.Value.Position.Y);
                if (info.Value.IsDead)
                {
                    this.Controls.Remove(pacmen[info.Key]);
                }
                switch (info.Value.Direction)
                {
                    case Direction.LEFT:
                        ((PictureBox)pacmen[info.Key]).Image = Properties.Resources.Left;
                        break;
                    case Direction.RIGHT:
                        ((PictureBox)pacmen[info.Key]).Image = Properties.Resources.Right;
                        break;
                    case Direction.UP:
                        ((PictureBox)pacmen[info.Key]).Image = Properties.Resources.Up;
                        break;
                    case Direction.DOWN:
                        ((PictureBox)pacmen[info.Key]).Image = Properties.Resources.down;
                        break;
                    case Direction.NONE:
                        break;
                    default:
                        break;
                }

                score[info.Key] = info.Value.Score;
            }

            //Update ghosts' positions
            foreach (KeyValuePair<Ghost, Position> ghost in state.GhostsPositions)
            {
                switch (ghost.Key)
                {
                    case Ghost.PINK:
                        pinkGhost.Location = new Point(ghost.Value.X, ghost.Value.Y);
                        break;
                    case Ghost.RED:
                        redGhost.Location = new Point(ghost.Value.X, ghost.Value.Y);
                        break;
                    case Ghost.YELLOW:
                        yellowGhost.Location = new Point(ghost.Value.X, ghost.Value.Y);
                        break;
                    default:
                        break;
                }
            }


            //Remove "eaten" coins
            foreach (string coinName in state.CoinsToRemove)
            {
                this.Controls.Remove(this.Controls.Find(coinName, false)[0]);
                this.CoinPositions.Remove(coinName);

            }

            //Generate LocalGameState
            if (fileInput)
            {
                try
                {
                    this.GameStates.Add(state.RoundNumber, new LocalGameState(state, this.CoinPositions.Values.ToList()));
                }
                catch (ArgumentException)
                {
                    this.GameStates[state.RoundNumber] = new LocalGameState(state, this.CoinPositions.Values.ToList());
                }

            }

        }

        private void AddPacman(string pId, Position pos)
        {
            PictureBox pacman = new PictureBox
            {
                BackColor = Color.Transparent,
                Image = Properties.Resources.Left,
                Location = new Point(pos.X, pos.Y),
                Margin = new Padding(0),
                Name = pId,
                Size = new Size(25, 25),
                SizeMode = PictureBoxSizeMode.StretchImage,
                TabIndex = 4,
                TabStop = false
            };
            this.Controls.Add(pacman);
            this.pacmen.Add(pId, pacman);

        }


        private void keyisdown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left)
            {
                goleft = true;
            }
            if (e.KeyCode == Keys.Right)
            {
                goright = true;
            }
            if (e.KeyCode == Keys.Up)
            {
                goup = true;
            }
            if (e.KeyCode == Keys.Down)
            {
                godown = true;
            }
            if (e.KeyCode == Keys.Enter)
            {
                tbMsg.Enabled = true;
                tbMsg.Focus();
            }
        }

        private void keyisup(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left)
            {
                goleft = false;
            }
            if (e.KeyCode == Keys.Right)
            {
                goright = false;
            }
            if (e.KeyCode == Keys.Up)
            {
                goup = false;
            }
            if (e.KeyCode == Keys.Down)
            {
                godown = false;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            label1.Text = "Score: " + score[this.PId];
            if (isDead) return;

            if (fileInput)
            {
                try
                {
                    server.Move(this.PId, inputMoves.First().Value);
                }
                catch (SocketException)
                {
                    tbChat.AppendText("Could not connect to server.");
                    tbChat.AppendText(Environment.NewLine);
                    timer1.Stop();
                    return;
                }
                inputMoves.Remove(inputMoves.First().Key);
                if (inputMoves.Count == 0)
                {
                    fileInput = false;
                }
            }
            else
            {
                try
                {
                    if (goleft)
                    {
                        server.Move(this.PId, Direction.LEFT);
                    }
                    if (goright)
                    {
                        server.Move(this.PId, Direction.RIGHT);
                    }
                    if (goup)
                    {
                        server.Move(this.PId, Direction.UP);
                    }
                    if (godown)
                    {
                        server.Move(this.PId, Direction.DOWN);
                    }
                }
                catch (SocketException)
                {
                    tbChat.AppendText("Could not connect to server.");
                    tbChat.AppendText(Environment.NewLine);
                    timer1.Stop();
                    return;
                }

            }
        }


        private void tbMsg_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                tbMsg.Enabled = false; Focus();
                if (!freeze)
                {
                    string message = tbMsg.Text + Environment.NewLine;

                    tbChat.AppendText(String.Format("[{0}] {1}", PId, message));
                    tbMsg.Clear();

                    timestamps[this.PId]++;
                    if (message.Length > 0)
                    {
                        foreach (KeyValuePair<string, ExternalClient> client in clients)
                        {
                            if (client.Value.Disconnected) continue;
                            Thread thread = new Thread(() => ChatSendMessage(client.Key, message, timestamps));
                            thread.Start();
                        }

                    }
                }
            }
        }
    }

    class ChatMessage
    {
        public string Content { get; set; }
        public Dictionary<string, int> Vector { get; set; }
        public string Origin { get; set; }

        public ChatMessage(string origin, string content, Dictionary<string, int> vector)
        {
            this.Content = content;
            this.Vector = vector;
            this.Origin = origin;
        }

    }


}
