﻿using RemoteServices;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Client
{
    static class Program
    {

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new ClientForm(args));

        }
    }

    class ClientServices : MarshalByRefObject, IClientServices
    {
        bool freeze = false;
        private object unfreezing = new object();

        private Queue<MethodInvoker> toProcess = new Queue<MethodInvoker>();


        public void SendMsg(string origin, string message, Dictionary<string, int> vector)
        {
            ClientForm form = (ClientForm)Application.OpenForms[0];
            MethodInvoker mI = delegate () { form.AddMessage(origin, message, vector); };
            if (freeze) toProcess.Enqueue(mI);
            else form.Invoke(mI);
        }

        public void SignalFreeze()
        {
            freeze = true;
            ClientForm form = (ClientForm)Application.OpenForms[0];
            MethodInvoker mI = delegate () { form.BlockChat(); };
            form.Invoke(mI);
        }

        public void SignalUnFreeze()
        {
            lock (unfreezing)
            {
                ClientForm form = (ClientForm)Application.OpenForms[0];
                for (int i = 0; i < toProcess.Count; i++)
                {
                    form.Invoke(toProcess.Dequeue());
                }
                freeze = false;
                form.Invoke(new MethodInvoker(() => { form.UnBlockChat(); }));
            }
        }

        public void SendGameInfo(Dictionary<string, Position> positions)
        {
            ClientForm form = (ClientForm)Application.OpenForms[0];
            MethodInvoker mI = delegate () { form.GameStartInfo(positions); };
            if (freeze) toProcess.Enqueue(mI);
            else form.Invoke(mI);
        }

        public void Ping()
        {
        }


        public void SignalDelay(string destPID)
        {
            ClientForm form = (ClientForm)Application.OpenForms[0];
            MethodInvoker mI = delegate () { form.DelayMessage(destPID); };
            form.Invoke(mI);
        }

        public void SendState(GameStateChanges state)
        {
            ClientForm form = (ClientForm)Application.OpenForms[0];
            MethodInvoker mI = delegate () { form.UpdateGameState(state); };
            if (freeze) toProcess.Enqueue(mI);
            else form.Invoke(mI);
        }

        public void SignalGameEnd(string winner, int points)
        {
            ClientForm form = (ClientForm)Application.OpenForms[0];
            MethodInvoker mI = delegate () { form.GameEnd(winner, points); };
            if (freeze) toProcess.Enqueue(mI);
            else form.Invoke(mI);
        }

        public void LocalState(int roundNumber)
        {
            ClientForm form = (ClientForm)Application.OpenForms[0];
            MethodInvoker mI = delegate () { form.LocalState(roundNumber); };
            form.Invoke(mI);
        }

        public void SignalGameStart()
        {
            ClientForm form = (ClientForm)Application.OpenForms[0];
            MethodInvoker mI = delegate () { form.StartGame(); };
            if (freeze) toProcess.Enqueue(mI);
            else form.Invoke(mI);
        }

        public void NotifyNewServer(string endpoint)
        {
            ClientForm form = (ClientForm)Application.OpenForms[0];
            MethodInvoker mI = delegate () { form.ReconnectToServer(endpoint); };
            form.Invoke(mI);
            //TODO: freeze
        }
    }

}
