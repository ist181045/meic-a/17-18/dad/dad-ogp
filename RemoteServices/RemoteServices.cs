using System;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;

namespace RemoteServices
{
    public interface ICommonServices
    {
        void SignalFreeze();
        void SignalUnFreeze();
        [OneWay]
        void SignalDelay(string destPID);
        void LocalState(int roundNumber);
    }
    public interface IServerServices : ICommonServices
    {
        Dictionary<string, ExternalClient> getConnectedClients();
        string getPID();
        void AddClient(string endpoint, string name);
        void Move(string client, Direction direction);
        void ImAlive(string pId);
        void updateInternalState(GameStateChanges newState);
        void UpdateClientInfo(KeyValuePair<string, ExternalClient> client);
        void RegisterReplica(string pId, IServerServices newReplica);
        void UpdateReplicas(Dictionary<string, ServerReplica> servers);
        void SignalRingStart();
        void ChooseLeader(string leaderPId);
    }

    public interface IClientServices : ICommonServices
    {
        void SendMsg(string origin, string message, Dictionary<string, int> vector);
        void SendGameInfo(Dictionary<string, Position> positions);
        //[OneWay] ?MAYBE?
        void Ping();
        void SendState(GameStateChanges state);
        void SignalGameStart();
        void SignalGameEnd(string winner, int points);
        void NotifyNewServer(string endpoint);
    }

    public interface IPCSServices
    {

        bool StartServer(string pId, string serverURL, string mSecRound, string numPlayers);
        bool StartReplica(string pId, string serverURL, string mSecRound, string numPlayers, string primaryURL);
        bool StartClient(string pId, string clientURL, string serverURL, string mSecRound, string numPlayers, string filename);
        bool FreezeProcess(string pId);
        bool CrashProcess(string pId);
        bool UnFreezeProcess(string pId);
        bool InjectDelay(string srcPID, string descPID);
        bool LocalState(string pId, int roundNumber);
        void Exit();
    }

    [Serializable]
    public class ExternalClient
    {
        public IClientServices ClientServices { get; set; }
        public bool Delay { get; set; }
        public bool Disconnected { get; set; }
        public int Tries { get; set; }
        public ExternalClient(IClientServices clientServices)
        {
            this.ClientServices = clientServices;
            this.Delay = false;
            this.Disconnected = false;
            this.Tries = 0;
        }
    }

    [Serializable]
    public class ServerReplica
    {
        public IServerServices ServerServices { get; set; }
        public bool Disconnected { get; set; }
        public bool IsPrimary { get; set; }
        public bool IsAlive { get; set; }
        public int Tries { get; set; }

        public ServerReplica(IServerServices server, bool primary)
        {
            this.ServerServices = server;
            this.Disconnected = false;
            this.IsPrimary = primary;
            this.IsAlive = true;
            this.Tries = 0;
        }
    }

    [Serializable]
    public class GameState
    {
        public int RoundNumber { get; set; }
        public Dictionary<string, ClientInfo> Clients { get; set; }
        public Dictionary<Ghost, Position> GhostsPositions { get; set; }

        public GameState(int number, Dictionary<string, ClientInfo> clients, Dictionary<Ghost, Position> ghosts)
        {
            this.RoundNumber = number;
            this.Clients = clients;
            this.GhostsPositions = ghosts;
        }
    }
    [Serializable]
    public class GameStateChanges : GameState
    {
        public List<string> CoinsToRemove { get; set; }

        public GameStateChanges(
            int number,
            Dictionary<string, ClientInfo> clients,
            Dictionary<Ghost, Position> ghosts,
            List<string> coins) : base(number, clients, ghosts)
        {
            this.CoinsToRemove = coins;
        }

    }

    public class LocalGameState : GameState
    {
        public List<Position> ExistingCoins { get; set; }

        public LocalGameState(
            int number,
            Dictionary<string, ClientInfo> clients,
            Dictionary<Ghost, Position> ghosts,
            List<Position> coins) : base(number, clients, ghosts)
        {
            this.ExistingCoins = coins;
        }

        public LocalGameState(GameStateChanges state, List<Position> coins) : base(state.RoundNumber, state.Clients, state.GhostsPositions)
        {
            this.ExistingCoins = coins;
        }

        public override string ToString()
        {
            string res = "";
            foreach (KeyValuePair<Ghost, Position> ghost in GhostsPositions)
            {
                res += String.Format("M {0} {1}", ghost.Value.X, ghost.Value.Y);
                res += Environment.NewLine;
            }

            foreach (KeyValuePair<string, ClientInfo> client in Clients)
            {
                res += String.Format("{0} {1} {2} {3}",
                    client.Key, client.Value.IsDead ? 'L' : 'P', client.Value.Position.X, client.Value.Position.Y);
                res += Environment.NewLine;
            }

            foreach (Position coin in ExistingCoins)
            {
                res += String.Format("C {0} {1}", coin.X, coin.Y);
                res += Environment.NewLine;
            }

            return res;
        }
    }

    [Serializable]
    public class ClientInfo
    {
        public Position Position { get; set; }
        public int Score { get; set; }
        public bool IsDead { get; set; }
        public Direction Direction { get; set; }

        public ClientInfo(Position pos, int score, bool isDead, Direction direction)
        {
            Position = pos;
            Score = score;
            IsDead = isDead;
            Direction = direction;
        }
    }

    [Serializable]
    public class Position
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Position(int x, int y)
        {
            X = x;
            Y = y;
        }
    }

    public enum Direction
    {
        LEFT,
        RIGHT,
        UP,
        DOWN,
        NONE
    }
    public enum Ghost
    {
        PINK,
        RED,
        YELLOW
    }

}